package com.visby.visby_patient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dropbox.core.util.StringUtil;

import soup.neumorphism.NeumorphCardView;

public class SignIn extends AppCompatActivity {

    private EditText mUserName, mPassword;
    private NeumorphCardView mSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        Log.e("SignIn","OnCreate()");

        mUserName = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);

        mSignIn = findViewById(R.id.BtnSignIn);

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    Intent intent = new Intent(SignIn.this, MainActivity.class);
                    intent.putExtra("USER_NAME", mUserName.getText().toString());
                    startActivity(intent);
                }

            }
        });
    }

    private boolean validate(){
        Log.e("SignIn","validate()");
        boolean validate = true;
        if(TextUtils.isEmpty(mUserName.getText().toString())){

            mUserName.setError("Please enter user Name");
            validate = false;

        }

//        if(TextUtils.isEmpty(mPassword.getText().toString())){
//
//            mPassword.setError("Please enter user Name");
//            validate = false;
//
//        }
        return validate;
    }
}