package com.visby.visby_patient.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.islamkhsh.CardSliderAdapter;
import com.visby.visby_patient.R;
import com.visby.visby_patient.interfaceListener.Appointment_Interface;
import com.visby.visby_patient.model.Appointment;
import com.visby.visby_patient.model.Result;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AppointmentAdapter extends CardSliderAdapter<AppointmentAdapter.AppointmentViewHolder> {

    private ArrayList<Appointment> mAppointments;
    private Appointment_Interface mAppointment_interface;

    public AppointmentAdapter(ArrayList<Appointment> appointments, Appointment_Interface mAppointment_interface) {
        mAppointments = appointments;
        this.mAppointment_interface = mAppointment_interface;
    }

    @Override
    public void bindVH(AppointmentViewHolder appointmentViewHolder, int i) {

        if(i == 0) {
            appointmentViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAppointment_interface.onclickApointment();
                }
            });
        }

        appointmentViewHolder.mDoctorName.setText(mAppointments.get(i).getName());
        appointmentViewHolder.mVisitDateAndTime.setText(mAppointments.get(i).getDateAndTime());
        appointmentViewHolder.mAddress.setText(mAppointments.get(i).getAddress());

    }

    @NonNull
    @Override
    public AppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_item_view, parent, false);
        return new AppointmentAdapter.AppointmentViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mAppointments.size();
    }

    class AppointmentViewHolder extends RecyclerView.ViewHolder {

        TextView mDoctorName;
        TextView mVisitDateAndTime;
        TextView mAddress;
        public AppointmentViewHolder(View view) {
            super(view);
            mDoctorName = view.findViewById(R.id.doctor_name);
            mVisitDateAndTime = view.findViewById(R.id.visit_date_time);
            mAddress = view.findViewById(R.id.address);
        }
    }
}
