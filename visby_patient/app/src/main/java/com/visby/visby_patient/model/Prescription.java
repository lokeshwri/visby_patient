package com.visby.visby_patient.model;

public class Prescription {

    private String medicen;
    private String consumption;
    private String test;
    private String prescribedDoctor;

    public String getMedicen() {
        return medicen;
    }

    public void setMedicen(String medicen) {
        this.medicen = medicen;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getPrescribedDoctor() {
        return prescribedDoctor;
    }

    public void setPrescribedDoctor(String prescribedDoctor) {
        this.prescribedDoctor = prescribedDoctor;
    }
}
