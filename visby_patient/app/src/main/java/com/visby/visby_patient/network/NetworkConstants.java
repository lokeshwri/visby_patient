package com.visby.visby_patient.network;

public interface NetworkConstants {
//        http://49.206.24.239:8080/PopulationCareV2LMS/getCountryList
//    String REST_BASE_URL = "http://49.206.24.239:8080/PopulationCareV2LMS/";
//        String REST_BASE_URL = "http://192.168.1.167:9092/PopulationCarev2/";
//        String REST_BASE_URL = "http://192.168.43.173:3000/";
//        String REST_BASE_URL = "http://192.168.1.3:3000/";
//        String REST_BASE_URL = "http://192.168.1.31:9090/PopulationCarev2/";
//        String REST_BASE_URL = "http://106.51.137.105:8080/PopulationCarev2/";
//        String REST_BASE_URL = "http://192.168.1.45:9090/PopulationCarev2/";
//        String REST_BASE_URL_LMS = "http://192.168.1.45:8086";
//        String REST_BASE_URL_LMS = "http://13.232.237.236:8086/";
//        String REST_BASE_URL = "http://13.232.237.236:8080/PopulationCarev2/";
//        String REST_BASE_URL = "http://192.168.1.150:8080/PopulationCareV2LMS/";
//        String REST_BASE_URL_LMS = "http://192.168.1.150:9090/LMS/";

        String REST_BASE_URL = "http://13.235.235.100:8080/PopulationCareV2/";

//        String REST_BASE_URL = "http://13.235.14.189:8080/PopulationCareV2LMS/";

//        String REST_BASE_URL = "http://192.168.1.150:8080/PopulationCareV2LMS/";
//        String REST_BASE_URL = "http://192.168.1.45:9090/PopulationCarev2/";
//        String REST_BASE_URL = "http://49.206.24.239:8080/PopulationCareV2LMS/";
//        String REST_BASE_URL_LMS = "http://49.206.24.239:9090/";
//        String REST_BASE_URL_LMS = "http://192.168.1.45:8080/";
//        String REST_BASE_URL_LMS = "https://lms.wenzins.live/LMS/";
//        String REST_BASE_URL = "https://wrizto.com/PopulationCareLms/";
//        String REST_BASE_URL_LMS = "http://49.206.24.239:9090/LMS/";


//        String CHECK_USER_AVAILABILITY = "checkUserAvailability";
        String CHECK_FOR_CALL = "checkForCall";
        String  INTIATE_ADN_CLOSE_CALL ="intiateAndCloseCall";
        String GETLICENSEKEY = "getLicenseKey";
//        String DOCTOR_SYNCH_SUMMERY = "doctorSyncSummary";
//        String SYNCH_DATA = "syncData";
//        String GET_SYNCH_SUMMERY = "getSyncSummary";
        String CONTEXT_PATH = "PopulationCarev2/";
//        String CONTEXT_PATH = "PopulationCare/";
//        String CLIENT_SET_UP = "clientSetUpv2";
        String CLIENT_SET_UP = "clientSetUpv3";
//        String GET_APP_EXPIRY = "getAppExpiryDate";
        String CHECK_FOR_CALL_V2 = "checkForCallv2";
//        String CHECK_FOR_CALL_V3 = "checkForCallv3";
//        String INTIATEANDCLOSECALLV3 = "intiateAndCloseCallv3";

//        String CHECK_FOR_CALL_V4 = "checkForCallv4";
//        String INTIATEANDCLOSECALLV3 = "intiateAndCloseCallv4";
        String REGISTER_SAMVITA_USER = "RegisterNurseUser";
        String GET_LICENCE_KEY_STATUS = "getLicenceKeyStatus";

        String Auth = "api/auth/login";

        String CHECK_USER_AVAILABILITY = "checkUserAvailabilitySamvitaLatest";
        String CHECK_FOR_CALL_V4 = "checkForCallv4SamvitaLatest";
        String INTIATEANDCLOSECALLV4 = "intiateAndCloseCallv4SamvitaLatest";
        String DOCTOR_SYNCH_SUMMERY = "doctorSyncSummarySamvitaLatest";
        String GET_SYNCH_SUMMERY = "getSyncSummarySamvitaLatest";
        String GET_APP_EXPIRY = "getAppExpiryDateSamvitaLatest";
        String SYNCH_DATA = "syncDataSamvitaLatest";
        String GET_CONSUMER_VITALS = "getConsumerVitals";
        String GET_PATIENT_LIST = "getPatientListForVisbyDoctor";

}
